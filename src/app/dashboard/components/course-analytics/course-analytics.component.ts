import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from "@themost/angular/client";

@Component({
  selector: 'app-course-analytics',
  templateUrl: './course-analytics.component.html',
  styles: []
})
export class CourseAnalyticsComponent implements OnInit {

    constructor(private context:AngularDataContext) {

    }

    // Doughnut
    public loading: boolean = true;
    public doughnutChartLabels:string[] = [];
    public doughnutChartData:number[] = [];
    public doughnutChartType:string = 'doughnut';

    // events
    public chartClicked(e:any):void {
        //
    }

    public chartHovered(e:any):void {
        //
    }

    ngOnInit(): void {
        this.context.model('students/me/courses')
            .select('courseType/name as courseTypeName','count(id) as count')
            .groupBy('courseType/name')
            .where('grade').greaterOrEqual(0.5)
            .getItems().then((res)=> {
                this.doughnutChartLabels = res.value.map(x=> {
                    return x.courseTypeName;
                });
                this.doughnutChartData = res.value.map(x=> {
                    return x.count;
                });
                this.loading = false;
        });
    }

}
