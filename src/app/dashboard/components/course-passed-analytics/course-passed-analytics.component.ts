import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from "@themost/angular/client";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-course-passed-analytics',
  templateUrl: './course-passed-analytics.component.html',
  styles: []
})
export class CoursePassedAnalyticsComponent implements OnInit {

    constructor(private context:AngularDataContext, private translateService: TranslateService) {

    }

    // Doughnut
    public loading: boolean = true;
    public doughnutChartLabels:string[] = [];
    public doughnutChartData:number[] = [];
    public doughnutChartColors: any[] =
        [
            {
                backgroundColor: 'rgba(177,200,84,0.2)',
                borderColor: 'rgba(106,185,236,1)'
            },
            {
                backgroundColor: 'rgba(24,200,84,0.2)',
                borderColor: 'rgba(24,185,236,1)'
            }
        ];
    public doughnutChartType:string = 'doughnut';

    // events
    public chartClicked(e:any):void {
        //
    }

    public chartHovered(e:any):void {
        //
    }

    ngOnInit(): void {


        this.context.model('students/me/courses')
            .select('isPassed','count(id) as count')
            .groupBy('isPassed')
            .where('grade').notEqual(null)
            .getItems().then((res)=> {
                this.translateService.get(['SuccessedCourses', 'FailedCourses'])
                    .subscribe((translations)=> {
                        this.doughnutChartLabels = res.value.map(x=> {
                            return x.isPassed ? translations.SuccessedCourses : translations.FailedCourses;
                        });
                        this.doughnutChartData = res.value.map(x=> {
                            return x.count;
                        });
                        this.loading = false;
                    });
        });
    }

}
