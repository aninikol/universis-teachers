import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {CourseAnalyticsComponent} from './components/course-analytics/course-analytics.component';
import {TranslateModule} from "@ngx-translate/core";
import {MostModule} from "@themost/angular/module";
import {CommonModule} from "@angular/common";
import { CoursePassedAnalyticsComponent } from './components/course-passed-analytics/course-passed-analytics.component';
import { AveragePerPeriodComponent } from './components/average-per-period/average-per-period.component';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartsModule,
        TranslateModule,
        MostModule
    ],
    declarations: [DashboardComponent,
        CourseAnalyticsComponent,
        CoursePassedAnalyticsComponent,
        AveragePerPeriodComponent,
        AveragePerPeriodComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {

}
