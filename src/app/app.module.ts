import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {environment} from '../environments/environment';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AngularDataContext, ClientDataContextConfig, DATA_CONTEXT_CONFIG} from "@themost/angular/client";
import {ConfigurationService} from "./shared/services/configuration.service";
import {SharedModule} from "./shared/shared.module";
import {ErrorModule} from "./error/error.module";
import {AuthModule} from './auth/auth.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';




// Routing Module
import {AppRoutingModule} from './app.routing';
import {BreadcrumbsComponent} from "./layouts/breadcrumb.component";

// Import coreui components
import {
//  AppAsideComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './shared/coreui/components';

// Import coreui directives
import {
//  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './shared/coreui/directives';

const APP_COMPONENTS = [
//  AppAsideComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
];

const APP_DIRECTIVES = [
//  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
];

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    BreadcrumbsComponent,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule,
    AuthModule,
    AppRoutingModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot()

  ],
  providers: [
    {
            provide: DATA_CONTEXT_CONFIG, useValue: {
              base: "/",
              options: {
                  useMediaTypeExtensions: false
              }
            }
    },
    AngularDataContext,
    {
            provide: APP_INITIALIZER,
            useFactory: (config: ConfigurationService) => () => config.load(),
            deps: [ConfigurationService],
            multi: true
    },
    {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
    }
    ],
  bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(private _translateService: TranslateService, private _configurationService: ConfigurationService) {
      //
    }
}
