import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import {LogoutComponent} from "./auth/components/logout/logout.component";
import {ErrorBaseComponent, HttpErrorComponent} from "./error/components/error-base/error-base.component";
import {AuthGuard} from "./auth/guards/auth.guard";
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LangComponent} from "./shared/lang-component";

export const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'error',
    component: ErrorBaseComponent
  },
  {
    path: 'error/:status',
    component: HttpErrorComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'lang/:id',
    component: LangComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'classes',
        loadChildren: './classes/classes.module#ClassesModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule'
      },
      {
        path: 'exams',
        loadChildren: './exams/exams.module#ExamsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
