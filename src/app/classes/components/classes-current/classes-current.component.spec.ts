import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesCurrentComponent } from './classes-current.component';

describe('ClassesCurrentComponent', () => {
  let component: ClassesCurrentComponent;
  let fixture: ComponentFixture<ClassesCurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesCurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesCurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
