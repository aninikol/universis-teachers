import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
