import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import {ConfigurationService} from "./services/configuration.service";
import { MsgboxComponent } from './msgbox/msgbox.component';
import { LangswitchComponent } from './langswitch/langswitch.component';
import {LocalizedDatePipe} from "./localized-date.pipe";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {LangComponent} from "./lang-component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
      BsDropdownModule
  ],
  providers: [
      ConfigurationService
  ],
  declarations: [
  LocalizedDatePipe,
  MsgboxComponent,
      LangComponent,
  LangswitchComponent],
  exports: [
    MsgboxComponent,
    LangswitchComponent,
      LangComponent,
    LocalizedDatePipe]
})
export class SharedModule { }
