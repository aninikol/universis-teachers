import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

import { ExamsRoutingModule } from './exams-routing.module';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';

@NgModule({
  imports: [
    CommonModule,
    ExamsRoutingModule,
    TranslateModule
  ],
  declarations: [ExamsHomeComponent]
})
export class ExamsModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/exams.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
