import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsHomeComponent } from './exams-home.component';

describe('ExamsHomeComponent', () => {
  let component: ExamsHomeComponent;
  let fixture: ComponentFixture<ExamsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
