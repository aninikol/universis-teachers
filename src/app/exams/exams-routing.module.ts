import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {ExamsHomeComponent} from './components/exams-home/exams-home.component';

const routes: Routes = [
  {
    path: '',
    component: ExamsHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamsRoutingModule { }
